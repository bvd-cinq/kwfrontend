package view

import model.Budget

class BudgetView(private val budget: Budget) {
    fun toHtmlString(): String {
        return "Budget left: ${budget.amount}"
    }
}
