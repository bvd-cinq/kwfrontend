package model

data class Budget(
    var amount: Double = 0.0
)