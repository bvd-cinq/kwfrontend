package model

data class WishlistItem(
    val itemName: String,
    var amount: Double = 0.0
)
