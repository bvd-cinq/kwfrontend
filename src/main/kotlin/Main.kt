import kotlinx.browser.*
import kotlinx.html.*
import kotlinx.html.dom.*
import model.Budget
import view.BudgetView

fun main() {
    document.title = "Savings Wishlist"
    document.body!!.append.div {
        this.style = "margin: auto;"
        div {
            h1 { +"Savings Wishlist" }
        }
        div {
            id = "budget"
            +BudgetView(Budget()).toHtmlString()
            style = "display: flex; justify-content: center;"
        }
    }

    document.body!!.style.display = "flex"
    document.body!!.style.alignContent = "center"
}
